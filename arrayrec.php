<?php


class arrayrec
{

    /*
     * Recursive function that read the array fields and save the values in session array
     *
     * @param $value the array variable to check
     * @param $aux the old fields that has been seved before
     * @param $parent the parent key of array
     *
     */
    private function es_un_array($value, $aux, $parent){

        foreach ($value as $k => $val){
            if(!is_array($val)){
                $_SESSION['array_content'][$parent."#".$k] = $val;
            }else{
                $this->es_un_array($val, $aux, $parent."#".$k);
            }
        }
        return $aux;
    }

    /*
     * Function that reads an array and determine if the field is array to check or save
     *
     * @param $values the initial array multidimensional
     *
     * @return array key-value, with multidimensional arrays keys concatenated
     */
    public function recursive_fiels(array $values){
        $first_level = array();
        foreach($values as $key => $value){
            if (!is_array($value) && !is_object($value)){
                $first_level[$key] = $value;
            }
            if(is_array($value)){
                $this->es_un_array($value, [], $key);

            }
        }
        $aux2 = array_merge($first_level,$_SESSION['array_content']);
        return $aux2;
    }

}
